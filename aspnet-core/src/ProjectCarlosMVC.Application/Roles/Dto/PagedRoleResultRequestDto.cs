﻿using Abp.Application.Services.Dto;

namespace ProjectCarlosMVC.Roles.Dto
{
    public class PagedRoleResultRequestDto : PagedResultRequestDto
    {
        public string Keyword { get; set; }
    }
}

