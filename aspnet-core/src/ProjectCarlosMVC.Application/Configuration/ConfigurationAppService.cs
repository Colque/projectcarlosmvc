﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using ProjectCarlosMVC.Configuration.Dto;

namespace ProjectCarlosMVC.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : ProjectCarlosMVCAppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
