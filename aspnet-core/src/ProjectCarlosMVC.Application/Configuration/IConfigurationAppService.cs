﻿using System.Threading.Tasks;
using ProjectCarlosMVC.Configuration.Dto;

namespace ProjectCarlosMVC.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
