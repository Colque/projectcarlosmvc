﻿using System.Threading.Tasks;
using Abp.Application.Services;
using ProjectCarlosMVC.Sessions.Dto;

namespace ProjectCarlosMVC.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
