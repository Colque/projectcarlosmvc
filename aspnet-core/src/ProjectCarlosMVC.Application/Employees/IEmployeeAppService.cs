﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using ProjectCarlosMVC.Employees.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCarlosMVC.Employees
{
    public interface IEmployeeAppService : IApplicationService
    {
        Task<ListResultDto<Employee>> GetAll();

        Task<Employee> Create(EmployeeDto input);

        Task<Employee> GetEntityByIdAsync(string id);
    }
}
