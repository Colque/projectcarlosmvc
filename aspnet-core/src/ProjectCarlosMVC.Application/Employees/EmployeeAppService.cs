﻿using Abp.Application.Services.Dto;
using Abp.Domain.Entities;
using Abp.Domain.Repositories;
using Abp.Timing;
using Microsoft.AspNetCore.Identity;
using ProjectCarlosMVC.Employees.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCarlosMVC.Employees
{
    public class EmployeeAppService : ProjectCarlosMVCAppServiceBase, IEmployeeAppService
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeeAppService(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        public async Task<Employee> Create(EmployeeDto input)
        {
            var employee = new Employee();
            employee.Name = input.Name;
            employee.Surname = input.Surname;
            employee.Email = input.Email;
            employee.IsActive = input.IsActive;
            employee.RegisterDate = Clock.Now;
            employee.UpdatedDate = Clock.Now;
            await _employeeRepository.InsertAsync(employee);
            return employee;
        }

        public async Task<ListResultDto<Employee>> GetAll()
        {
            var tasks =  _employeeRepository.GetAll();

            return new ListResultDto<Employee>(
                ObjectMapper.Map<List<Employee>>(tasks)
            );
        }

        public async Task Update(EmployeeDto input)
        {
            var employee = await _employeeRepository.FirstOrDefaultAsync(e => e.Id == input.Id);
            employee.Name = input.Name;
            employee.Surname = input.Surname;
            employee.Email = input.Email;
            employee.IsActive = input.IsActive;
            employee.UpdatedDate = Clock.Now;
            await _employeeRepository.UpdateAsync(employee);
        }

        public async Task Delete(string id)
        {
            var employee = await _employeeRepository.FirstOrDefaultAsync(e => e.Id == id);
            await _employeeRepository.DeleteAsync(employee);
        }

        public async Task<Employee> GetEntityByIdAsync(string id)
        {
            var employee = await _employeeRepository.FirstOrDefaultAsync(e => e.Id == id);

            if (employee == null)
            {
                throw new EntityNotFoundException(typeof(Employee), id);
            }

            return employee;
        }
        protected void MapToEntity(EmployeeDto input, Employee user)
        {
            ObjectMapper.Map(input, user);
        }
    }
}
