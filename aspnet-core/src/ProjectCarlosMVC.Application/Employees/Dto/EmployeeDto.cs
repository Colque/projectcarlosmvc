﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ProjectCarlosMVC.Employees.Dto
{
    [AutoMapFrom(typeof(Employee))]
    public class EmployeeDto : EntityDto<long>
    {
        public string Id { get; set; }

        [StringLength(128)]
        public string Name { get; set; }

        [StringLength(128)]
        public string Surname { get; set; }

        [EmailAddress]
        [StringLength(256)]
        public string Email { get; set; }

        public bool IsActive { get; set; }

        public string FullName { get; set; }

        public DateTime? RegisterDate { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }
}
