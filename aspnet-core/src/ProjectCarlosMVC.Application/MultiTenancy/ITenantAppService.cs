﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using ProjectCarlosMVC.MultiTenancy.Dto;

namespace ProjectCarlosMVC.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedTenantResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}

