﻿using System.Threading.Tasks;
using Abp.Application.Services;
using ProjectCarlosMVC.Authorization.Accounts.Dto;

namespace ProjectCarlosMVC.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
    }
}
