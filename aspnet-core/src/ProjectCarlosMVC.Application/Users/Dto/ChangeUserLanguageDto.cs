using System.ComponentModel.DataAnnotations;

namespace ProjectCarlosMVC.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}