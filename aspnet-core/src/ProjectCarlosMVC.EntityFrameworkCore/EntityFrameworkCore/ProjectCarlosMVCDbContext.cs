﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using ProjectCarlosMVC.Authorization.Roles;
using ProjectCarlosMVC.Authorization.Users;
using ProjectCarlosMVC.MultiTenancy;
using ProjectCarlosMVC.Employees;

namespace ProjectCarlosMVC.EntityFrameworkCore
{
    public class ProjectCarlosMVCDbContext : AbpZeroDbContext<Tenant, Role, User, ProjectCarlosMVCDbContext>
    {
        /* Define a DbSet for each entity of the application */
        public DbSet<Employee> Employees { get; set; }
        public ProjectCarlosMVCDbContext(DbContextOptions<ProjectCarlosMVCDbContext> options)
            : base(options)
        {
        }
    }
}
