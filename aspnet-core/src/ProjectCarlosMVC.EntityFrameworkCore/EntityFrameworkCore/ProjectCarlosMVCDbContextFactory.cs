﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using ProjectCarlosMVC.Configuration;
using ProjectCarlosMVC.Web;

namespace ProjectCarlosMVC.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class ProjectCarlosMVCDbContextFactory : IDesignTimeDbContextFactory<ProjectCarlosMVCDbContext>
    {
        public ProjectCarlosMVCDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<ProjectCarlosMVCDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            ProjectCarlosMVCDbContextConfigurer.Configure(builder, configuration.GetConnectionString(ProjectCarlosMVCConsts.ConnectionStringName));

            return new ProjectCarlosMVCDbContext(builder.Options);
        }
    }
}
