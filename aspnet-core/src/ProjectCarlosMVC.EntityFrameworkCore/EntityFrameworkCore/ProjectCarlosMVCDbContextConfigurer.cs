using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace ProjectCarlosMVC.EntityFrameworkCore
{
    public static class ProjectCarlosMVCDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<ProjectCarlosMVCDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<ProjectCarlosMVCDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}
