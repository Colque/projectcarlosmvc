﻿using Abp.Configuration.Startup;
using Abp.Localization.Dictionaries;
using Abp.Localization.Dictionaries.Xml;
using Abp.Reflection.Extensions;

namespace ProjectCarlosMVC.Localization
{
    public static class ProjectCarlosMVCLocalizationConfigurer
    {
        public static void Configure(ILocalizationConfiguration localizationConfiguration)
        {
            localizationConfiguration.Sources.Add(
                new DictionaryBasedLocalizationSource(ProjectCarlosMVCConsts.LocalizationSourceName,
                    new XmlEmbeddedFileLocalizationDictionaryProvider(
                        typeof(ProjectCarlosMVCLocalizationConfigurer).GetAssembly(),
                        "ProjectCarlosMVC.Localization.SourceFiles"
                    )
                )
            );
        }
    }
}
