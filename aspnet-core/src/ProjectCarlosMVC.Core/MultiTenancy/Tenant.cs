﻿using Abp.MultiTenancy;
using ProjectCarlosMVC.Authorization.Users;

namespace ProjectCarlosMVC.MultiTenancy
{
    public class Tenant : AbpTenant<User>
    {
        public Tenant()
        {            
        }

        public Tenant(string tenancyName, string name)
            : base(tenancyName, name)
        {
        }
    }
}
