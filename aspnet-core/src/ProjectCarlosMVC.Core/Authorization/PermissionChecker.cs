﻿using Abp.Authorization;
using ProjectCarlosMVC.Authorization.Roles;
using ProjectCarlosMVC.Authorization.Users;

namespace ProjectCarlosMVC.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
