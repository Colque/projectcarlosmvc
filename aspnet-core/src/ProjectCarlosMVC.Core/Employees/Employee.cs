﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ProjectCarlosMVC.Employees
{
    [Table("Employees")]
    public class Employee : Entity
    {
        [StringLength(64)]
        public string Id { get; set; } = Guid.NewGuid().ToString();

        [StringLength(128)]
        public string Name { get; set; }

        [StringLength(128)]
        public string Surname { get; set; }

        [StringLength(256)]
        public string Email { get; set; }

        public DateTime? RegisterDate { get; set; }

        [StringLength(128)]
        public string RegisterBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [StringLength(128)]
        public string UpdatedBy { get; set; }

        public DateTime? DeletedDate { get; set; }

        [StringLength(128)]
        public string DeletedBy { get; set; }

        public bool IsActive { get; set; } = true;
    }
}
