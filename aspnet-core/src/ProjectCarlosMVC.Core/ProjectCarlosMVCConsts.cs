﻿namespace ProjectCarlosMVC
{
    public class ProjectCarlosMVCConsts
    {
        public const string LocalizationSourceName = "ProjectCarlosMVC";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;
    }
}
