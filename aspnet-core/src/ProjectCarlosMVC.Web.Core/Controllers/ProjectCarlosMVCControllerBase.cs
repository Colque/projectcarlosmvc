using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Identity;

namespace ProjectCarlosMVC.Controllers
{
    public abstract class ProjectCarlosMVCControllerBase: AbpController
    {
        protected ProjectCarlosMVCControllerBase()
        {
            LocalizationSourceName = ProjectCarlosMVCConsts.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
