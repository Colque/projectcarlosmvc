﻿using Abp.AutoMapper;
using ProjectCarlosMVC.Authentication.External;

namespace ProjectCarlosMVC.Models.TokenAuth
{
    [AutoMapFrom(typeof(ExternalLoginProviderInfo))]
    public class ExternalLoginProviderInfoModel
    {
        public string Name { get; set; }

        public string ClientId { get; set; }
    }
}
