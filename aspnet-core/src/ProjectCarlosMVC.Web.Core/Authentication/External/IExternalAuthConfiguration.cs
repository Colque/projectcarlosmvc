﻿using System.Collections.Generic;

namespace ProjectCarlosMVC.Authentication.External
{
    public interface IExternalAuthConfiguration
    {
        List<ExternalLoginProviderInfo> Providers { get; }
    }
}
