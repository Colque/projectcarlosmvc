﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using ProjectCarlosMVC.Configuration;

namespace ProjectCarlosMVC.Web.Host.Startup
{
    [DependsOn(
       typeof(ProjectCarlosMVCWebCoreModule))]
    public class ProjectCarlosMVCWebHostModule: AbpModule
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public ProjectCarlosMVCWebHostModule(IHostingEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(ProjectCarlosMVCWebHostModule).GetAssembly());
        }
    }
}
