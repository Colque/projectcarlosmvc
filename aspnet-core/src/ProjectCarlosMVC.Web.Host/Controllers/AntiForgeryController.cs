using Microsoft.AspNetCore.Antiforgery;
using ProjectCarlosMVC.Controllers;

namespace ProjectCarlosMVC.Web.Host.Controllers
{
    public class AntiForgeryController : ProjectCarlosMVCControllerBase
    {
        private readonly IAntiforgery _antiforgery;

        public AntiForgeryController(IAntiforgery antiforgery)
        {
            _antiforgery = antiforgery;
        }

        public void GetToken()
        {
            _antiforgery.SetCookieTokenAndHeader(HttpContext);
        }
    }
}
