﻿using ProjectCarlosMVC.Configuration.Ui;

namespace ProjectCarlosMVC.Web.Views.Shared.Components.RightSideBar
{
    public class RightSideBarViewModel
    {
        public UiThemeInfo CurrentTheme { get; set; }
    }
}
