﻿using Abp.AspNetCore.Mvc.ViewComponents;

namespace ProjectCarlosMVC.Web.Views
{
    public abstract class ProjectCarlosMVCViewComponent : AbpViewComponent
    {
        protected ProjectCarlosMVCViewComponent()
        {
            LocalizationSourceName = ProjectCarlosMVCConsts.LocalizationSourceName;
        }
    }
}
