﻿using Microsoft.AspNetCore.Mvc.Razor.Internal;
using Abp.AspNetCore.Mvc.Views;
using Abp.Runtime.Session;

namespace ProjectCarlosMVC.Web.Views
{
    public abstract class ProjectCarlosMVCRazorPage<TModel> : AbpRazorPage<TModel>
    {
        [RazorInject]
        public IAbpSession AbpSession { get; set; }

        protected ProjectCarlosMVCRazorPage()
        {
            LocalizationSourceName = ProjectCarlosMVCConsts.LocalizationSourceName;
        }
    }
}
