﻿using Microsoft.AspNetCore.Mvc;
using Abp.AspNetCore.Mvc.Authorization;
using ProjectCarlosMVC.Controllers;

namespace ProjectCarlosMVC.Web.Controllers
{
    [AbpMvcAuthorize]
    public class AboutController : ProjectCarlosMVCControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
	}
}
