﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using ProjectCarlosMVC.Authorization;
using ProjectCarlosMVC.Controllers;
using ProjectCarlosMVC.Users;
using ProjectCarlosMVC.Web.Models.Users;
using ProjectCarlosMVC.Users.Dto;
using ProjectCarlosMVC.Employees;
using ProjectCarlosMVC.Web.Models.Employees;

namespace ProjectCarlosMVC.Web.Controllers
{
    [AbpMvcAuthorize(PermissionNames.Pages_Users)]
    public class EmployeesController : ProjectCarlosMVCControllerBase
    {
        private readonly IEmployeeAppService _employeeAppService;

        public EmployeesController(IEmployeeAppService employeeAppService)
        {
            _employeeAppService = employeeAppService;
        }

        public async Task<ActionResult> Index()
        {
            var employees = (await _employeeAppService.GetAll()).Items; // Paging not implemented yet
            var model = new EmployeeListViewModel(employees);
            return View(model);
        }

        public async Task<Employee> Create(Employee input)
        {
            var task = ObjectMapper.Map<Employee>(input);
            return task;
        }

        public async Task<ActionResult> EditEmployeeModal(string employeeId)
        {
            var employee = await _employeeAppService.GetEntityByIdAsync(employeeId);
            var model = new EditEmployeeModalViewModel { Employee = employee };
            return View("_EditEmployeeModal", model);
        }
    }
}
