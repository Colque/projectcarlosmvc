﻿(function() {
    $(function() {

        var _employeeService = abp.services.app.employee;
        var _$modal = $('#EmployeeCreateModal');
        var _$form = _$modal.find('form');

        _$form.validate({
            rules: {
                Password: "required",
                ConfirmPassword: {
                    equalTo: "#Password"
                }
            }
        });

        $('#RefreshButton').click(function () {
            refreshUserList();
        });

        $('.delete-employee').click(function () {
            var employeeId = $(this).attr("data-employee-id");
            var employeeName = $(this).attr('data-employee-name');

            deleteEmployee(employeeId, employeeName);
        });

        $('.edit-employee').click(function (e) {
            var employeeId = $(this).attr("data-employee-id");

            e.preventDefault();
            $.ajax({
                url: abp.appPath + 'Employees/EditEmployeeModal?employeeId=' + employeeId,
                type: 'POST',
                contentType: 'application/html',
                success: function (content) {
                    $('#EmployeeEditModal div.modal-content').html(content);
                },
                error: function (e) { }
            });
        });

        _$form.find('button[type="submit"]').click(function (e) {
            e.preventDefault();

            if (!_$form.valid()) {
                return;
            }

            var employee = _$form.serializeFormToObject(); //serializeFormToObject is defined in main.js

            abp.ui.setBusy(_$modal);
            _employeeService.create(employee).done(function () {
                _$modal.modal('hide');
                location.reload(true); //reload page to see new user!
            }).always(function () {
                abp.ui.clearBusy(_$modal);
            });
        });

        _$modal.on('shown.bs.modal', function () {
            _$modal.find('input:not([type=hidden]):first').focus();
        });

        function refreshUserList() {
            location.reload(true); //reload page to see new user!
        }

        function deleteEmployee(employeeId, userName) {
            abp.message.confirm(
                abp.utils.formatString(abp.localization.localize('AreYouSureWantToDelete', 'ProjectCarlosMVC'), userName),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _employeeService.delete(employeeId).done(function () {
                            refreshUserList();
                        });
                    }
                }
            );
        }
    });
})();
