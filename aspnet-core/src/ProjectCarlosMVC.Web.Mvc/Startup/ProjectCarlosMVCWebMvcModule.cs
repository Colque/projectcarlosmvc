﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using ProjectCarlosMVC.Configuration;

namespace ProjectCarlosMVC.Web.Startup
{
    [DependsOn(typeof(ProjectCarlosMVCWebCoreModule))]
    public class ProjectCarlosMVCWebMvcModule : AbpModule
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public ProjectCarlosMVCWebMvcModule(IHostingEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void PreInitialize()
        {
            Configuration.Navigation.Providers.Add<ProjectCarlosMVCNavigationProvider>();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(ProjectCarlosMVCWebMvcModule).GetAssembly());
        }
    }
}
