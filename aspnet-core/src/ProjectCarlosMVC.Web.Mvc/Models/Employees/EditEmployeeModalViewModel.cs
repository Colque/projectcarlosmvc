using System.Collections.Generic;
using System.Linq;
using ProjectCarlosMVC.Employees;
using ProjectCarlosMVC.Employees.Dto;
using ProjectCarlosMVC.Roles.Dto;
using ProjectCarlosMVC.Users.Dto;

namespace ProjectCarlosMVC.Web.Models.Employees
{
    public class EditEmployeeModalViewModel
    {
        public Employee Employee { get; set; }
    }
}
