﻿using ProjectCarlosMVC.Employees;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectCarlosMVC.Web.Models.Employees
{
    public class EmployeeListViewModel
    {
        public IReadOnlyList<Employee> Employees { get; }

        public EmployeeListViewModel(IReadOnlyList<Employee> employees)
        {
            Employees = employees;
        }
    }
}
