﻿using System.Collections.Generic;
using ProjectCarlosMVC.Roles.Dto;

namespace ProjectCarlosMVC.Web.Models.Roles
{
    public class RoleListViewModel
    {
        public IReadOnlyList<RoleListDto> Roles { get; set; }

        public IReadOnlyList<PermissionDto> Permissions { get; set; }
    }
}
