using System.Collections.Generic;
using ProjectCarlosMVC.Roles.Dto;
using ProjectCarlosMVC.Users.Dto;

namespace ProjectCarlosMVC.Web.Models.Users
{
    public class UserListViewModel
    {
        public IReadOnlyList<UserDto> Users { get; set; }

        public IReadOnlyList<RoleDto> Roles { get; set; }
    }
}
