﻿using System.Collections.Generic;
using ProjectCarlosMVC.Roles.Dto;

namespace ProjectCarlosMVC.Web.Models.Common
{
    public interface IPermissionsEditViewModel
    {
        List<FlatPermissionDto> Permissions { get; set; }
    }
}